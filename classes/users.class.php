<?php

class User {
    private $id;
    private $firstname;
    private $lastname;
    private $email;
    private $password;
    private $code;
    private $user_type_id;
    private $is_admin;
    private $company_id;
    private $created_at;

    public function __construct($arr) {
        $this->email = $arr['email'];
        $this->password = $arr['password'];
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getFirstname() {
        return $this->firstname;
    }

    public function setFirstname($fn) {
        $this->firstname = $fn;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function setLastname($ln) {
        $this->lastname = $ln;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getPassword() {
        return md5($this->password);
    }

    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function getUserTypeId() {
        return $this->user_type_id;
    }

    public function setUserTypeId($us_id) {
        $this->user_type_id = $us_id;
    }

    public function getIsAdmin() {
        return $this->is_admin;
    }

    public function setIsAdmin($bool) {
        $this->is_admin = $bool;
    }

    public function getCompanyId() {
        return $this->company_id;
    }

    public function setCompanyId($co_id) {
        $this->company_id = $co_id;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

    public function setCreatedAt($timeAndDate) {
        $this->created_at = $timeAndDate;
    }

    public function getInfo() {
        return [
            $this->getId(),
            $this->getFirstname(),
            $this->getLastname(),
            $this->getEmail(),
            $this->getPassword(),
            $this->getCode(),
            $this->getUserTypeId(),
            $this->getIsAdmin(),
            $this->getCompanyId(),
            $this->getCreatedAt(),
          ];
    }

}
