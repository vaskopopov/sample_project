<?php


class Db {
    private $connection;
    
    public function __construct() {
        $host = 'localhost';
        $db = 'popovxyz_projectX';
        $username = 'popovxyz_developers';
        $password = 'developers123+';
        $charset = 'utf8';
        $options = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
        try {
        $this->connection = new PDO("mysql:host=$host;dbname=$db;charset=$charset", $username, $password, $options);
        } catch(PDOException $e) {
            $filename = "logs/log".date("y-m-d").".txt";
            $message = date("h:i:s")." ===> ".$ex->getMessage()."\n\n";
            file_put_contents($filename, $message, FILE_APPEND);
                
            echo "Service unavailable at the moment. Please try again later.";
            die();
        }
        
    }

    public function connectToDb() {
        return $this->connection;
    }

    

    public function getUserData($email, $password) {
        $sql = "SELECT * FROM `users` WHERE `email` = :email and `password` = :password";
        $stmt = $this->connectToDb()->prepare($sql);
        $stmt->execute(['email' => $email, 'password' => $password]);
        if($stmt->rowCount()) {
            $row = $stmt->fetch();
            return $row;
        }
        
    }
    
}