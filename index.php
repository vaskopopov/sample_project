<?php 

  require_once 'includes/check.login.php';
  $title='Responsive web app kit';
  require_once 'includes/header.php';
  // var_dump($_SESSION['user']);
  // die(); 
?>
  <body>
    <!-- Header start-->
    <header>
      <a href="index.php" class="brand pull-left"><img src="build/images/logo/logo-light.png" alt="" width="100" class="logo"><img src="build/images/logo/logo-sm-light.png" alt="" width="28" class="logo-sm"></a><a href="javascript:;" role="button" class="hamburger-menu pull-left"><span></span></a>
      
      <ul class="notification-bar list-inline pull-right">
        <li class="visible-xs"><a href="javascript:;" role="button" class="header-icon search-bar-toggle"><i class="ti-search"></i></a></li>
        
        <li class="dropdown hidden-xs"><a id="dropdownMenu2" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle header-icon lh-1 pt-15 pb-15">
            <div class="media mt-0">
              <div class="media-left avatar"><img src="build/images/users/04.jpg" alt="" class="media-object img-circle"><span class="status bg-success"></span></div>
              <div class="media-right media-middle pl-0">
                <p class="fs-12 text-base mb-0">Hi, <?= $_SESSION['user'][1]['firstname'] ?></p>
              </div>
            </div></a>
          <ul aria-labelledby="dropdownMenu2" class="dropdown-menu fs-12 animated fadeInDown">
            <li><a href="profile.php"><i class="ti-user mr-5"></i> My Profile</a></li>
            <li><a href="logout.php"><i class="ti-power-off mr-5"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Header end-->
    <div class="main-container">
      <!-- Main Sidebar start-->
      <aside class="main-sidebar">
        <div class="user">
          <div id="esp-user-profile" data-percent="65" style="height: 130px; width: 130px; line-height: 100px; padding: 15px;" class="easy-pie-chart"><img src="build/images/users/04.jpg" alt="" class="avatar img-circle"><span class="status bg-success"></span></div>
          <h4 class="fs-16 text-white mt-15 mb-5 fw-300"><?= $_SESSION['user'][1]['firstname']. " " . $_SESSION['user'][2]['lastname'] ?></h4>
          <p class="mb-0 text-muted">Designer</p>
        </div>
        <ul class="list-unstyled navigation mb-0">          
          <li class="panel"><a href="feedbacks.php"><i class="ti-layers-alt"></i>Evaluations </a></li>

          <?php if($_SESSION['user'][7]['is_admin'] == 1) { /** samo ako e admin pokazi gi slednite 2 menija */ ?>
            <li class="panel"><a href="tokens.php"><i class="ti-panel"></i>Tokens </a></li>
            <li class="panel"><a href="manage_users.php"><i class="ti-user"></i>Users </a></li>
          <?php } ?>
        </ul>
      </aside>
      <!-- Main Sidebar end-->
      <div class="page-container">
        <div class="page-header container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <h4 class="mt-0 mb-5">Welcome to Projecx <?= $_SESSION['user'][1]['firstname'] ?></h4>
              <p class="text-muted mb-0"><?= $_SESSION['user'][3]['email'] ?></p>
            </div>            
          </div>
        </div>
        <div class="page-content container-fluid">
          <div class="row">
            <div class="col-md-12">
                Ovde pokasno ke dodademe tabeli so feedbacks
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <p>Stuff to have dashbord:</p>
              <ul>
                <li>date of last database update</li>
                <li>button to Update the database</li>
                <li>total users</li>
                <li>add new user</li>
              </ul>  
            </div>
          </div>
        </div>
      </div>
      
    </div>

<?php
  require_once 'includes/scripts.php';
  require_once 'includes/footer.php';
?>