<?php 

  require_once 'includes/check.login.php';
  $title='Manage users';
  require_once 'includes/header.php';
  include_once 'includes/db.php';
?>
  <body>
    <!-- Header start-->
    <header>
      <a href="index.php" class="brand pull-left"><img src="build/images/logo/logo-light.png" alt="" width="100" class="logo"><img src="build/images/logo/logo-sm-light.png" alt="" width="28" class="logo-sm"></a><a href="javascript:;" role="button" class="hamburger-menu pull-left"><span></span></a>
      
      <ul class="notification-bar list-inline pull-right">
        <li class="visible-xs"><a href="javascript:;" role="button" class="header-icon search-bar-toggle"><i class="ti-search"></i></a></li>
        
        <li class="dropdown hidden-xs"><a id="dropdownMenu2" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle header-icon lh-1 pt-15 pb-15">
            <div class="media mt-0">
              <div class="media-left avatar"><img src="build/images/users/04.jpg" alt="" class="media-object img-circle"><span class="status bg-success"></span></div>
              <div class="media-right media-middle pl-0">
                <p class="fs-12 text-base mb-0">Hi, <?= $_SESSION['user_']['firstname'] ?></p>
              </div>
            </div></a>
          <ul aria-labelledby="dropdownMenu2" class="dropdown-menu fs-12 animated fadeInDown">
            <li><a href="profile.php"><i class="ti-user mr-5"></i> My Profile</a></li>
            <li><a href="logout.php"><i class="ti-power-off mr-5"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Header end-->
    <div class="main-container">
      <!-- Main Sidebar start-->
      <aside class="main-sidebar">
        <div class="user">
          <div id="esp-user-profile" data-percent="65" style="height: 130px; width: 130px; line-height: 100px; padding: 15px;" class="easy-pie-chart"><img src="build/images/users/04.jpg" alt="" class="avatar img-circle"><span class="status bg-success"></span></div>
          <h4 class="fs-16 text-white mt-15 mb-5 fw-300"><?= $_SESSION['user_']['firstname']. " " . $_SESSION['user_']['lastname'] ?></h4>
          <p class="mb-0 text-muted">Administrator</p>
        </div>
        <ul class="list-unstyled navigation mb-0">          
          <li class="panel"><a href="feedbacks.php"><i class="ti-layers-alt"></i>Evaluations </a></li>

          <?php if($_SESSION['user_']['is_admin']) { /** samo ako e admin pokazi gi slednite 2 menija */ ?>
            <li class="panel"><a href="tokens.php"><i class="ti-panel"></i>Tokens </a></li>
            <li class="panel"><a href="manage_users.php"><i class="ti-user"></i>Users </a></li>
            <ul>
              <li class="panel"><a href="addNewUser.php">Add new user</a></li>
            </ul>
          <?php } ?>
        </ul>
      </aside>
      <!-- Main Sidebar end-->
      <div class="page-container">
        <div class="page-header container-fluid">
          <div class="row">
            <div class="col-md-6">
              <h4 class="mt-0 mb-5">Welcome to ProjectX <?= $_SESSION['user_']['firstname'] ?></h4>
              <p class="text-muted mb-0"><?= $_SESSION['user_']['email'] ?></p>
            </div>            
            <div class="col-md-offset-3 col-md-3">
              <h4 class="mt-0 mb-5">Add new user</h4>
              <p class="text-muted mb-0"></p>
            </div>            
          </div>
        </div>
        <div class="page-content container-fluid">
          <div class="row">
            <div class="col-md-4">
                   <!-- Add new user form goes here -->

                <form action="" method="POST" name="addUser">
                    <div class="form-group">
                        <label for="firstname">First name</label>
                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter user's first name" onFocus="generate()">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Last name</label>
                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter user's last name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailInfo" placeholder="Enter email">
                        <small id="emailInfo" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="code">User's unique code</label>
                        <input type="text" class="form-control" id="code" name="code" placeholder="Enter two(or three) characters code for the user">
                        <small id="codeInfo" class="form-text text-muted"> </small>
                    </div>
                    <div class="form-group">
                        <label for="code">User's type</label>
                        <select class="form-control" name="user_type">
                          <option value="" disabled selected hidden>Select user type</option>
                          <?php
                              $types=$pdo->query("SELECT * from user_type;")->fetchAll();
                              for($i=0;$i<count($types);$i++){
                                  echo "<option value='{$types[$i]['id']}'>{$types[$i]['type']}</option>";
                              }
                          ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <p>Company: <?php
                            $companyID=$_SESSION['user_']['company_id'];
                            $company=$pdo->query("SELECT * from company WHERE id_company=$companyID;")->fetch();
                            echo "<b>{$company['name']}</b>";
                        ?>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="randomPassword">Password</label>
                        <input type="text" class="form-control" name="randomPassword" id="randomPassword"  placeholder="Password" readonly="true">
                        <input type="hidden" name="length" value="10">
                        <!-- <input type="button" class="btn btn-info btn-sm" value="Generate random password" onClick="generate();"> -->
                    </div>
                    <button type="submit" class="btn btn-primary" name="addUser" value="insertUser">Submit</button><br>
                </form>
                <?php
                    
                    require_once 'classes/database.class.php';

                    if($_SERVER['REQUEST_METHOD']=='POST' && $_POST['addUser']=="insertUser"){
                      try {
                        $sql="INSERT INTO users (`firstname`, `lastname`, `email`, `password`, `code`, `user_type_id`, `company_id`,`is_admin`) VALUES (:firstname, :lastname, :email, :password, :code, :user_type,:company, :is_admin);";

                        $dbInsert=new Db;
                        $password=$_POST['randomPassword'];
                        
                        //-------------------------------------
                        // password e staven vo SESSION za da moze da se iskoristi kako first time login samo
                        // ke odi preku metodot na forgoten password
                        session_start();
                        $_SESSION['otp']=$password;
                        //-----------------------------------------

                        $pass=md5($password);
                        $stmt=$dbInsert->connectToDb()->prepare($sql);
                        // $stmt=$pdo->prepare($sql);
                        $stmt->bindParam(':firstname',$_POST['firstname']);
                        $stmt->bindParam(':lastname',$_POST['lastname']);
                        $stmt->bindParam(':email',$_POST['email']);
                        $stmt->bindParam(':code',$_POST['code']);
                        $stmt->bindParam(':password',$pass);
                        $stmt->bindParam(':user_type',$_POST['user_type']);
                        $stmt->bindValue(':is_admin','0');
                        $stmt->bindParam(':company',$companyID);
                        // var_dump($stmt);die();
                        $passed=$stmt->execute();
                        if($passed){
                          echo "<p class='text-info'>User added successfully!</p>";
                          } else {
                          echo "<p class='text-danger'>There was some unexpected problem. Please try later!</p>";
                          // var_dump($dbInsert->connectToDb()->errorInfo());
                          // var_dump($pdo->errorInfo());
                          }
                      } catch (PDOException $ex){
                          $filename = "logs/log".date("y-m-d").".txt";
                          $message = date("h:i:s")." ===> ".$ex->getMessage()."\n\n";
                          file_put_contents($filename, $message, FILE_APPEND);
                      
                      
                          echo "Service unavailable at the moment. Please try again later.";
                          die();
                      }
                    }
                ?>
            </div>
          </div>
        </div>
      </div>
      
    </div>

<?php
  require_once 'includes/scripts.php';
  require_once 'includes/footer.php';
?>