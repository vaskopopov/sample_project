<?php

    require_once 'classes/database.class.php';


    if(isset($_GET['id'])){
        try
        {
            $id = $_GET['id'];
            $sql = "DELETE FROM users WHERE id_users = :id";
            $conn=new Db;
            $sh = $conn->connectToDb()->prepare($sql);
            $sh->bindParam(":id",$id);
            $sh->execute();
            
            header('Location: manage_users.php');

        } catch(PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }
