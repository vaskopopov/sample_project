<?php

try {
    $pdo = new PDO("mysql:host=localhost;dbname=popovxyz_projectX;charset=utf8", "popovxyz_developers", "developers123+", [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $ex) {
    
    $filename = "logs/log".date("y-m-d").".txt";
    $message = date("h:i:s")." ===> ".$ex->getMessage()."\n\n";
    file_put_contents($filename, $message, FILE_APPEND);


    echo "Service unavailable at the moment. Please try again later.";
    die();
}