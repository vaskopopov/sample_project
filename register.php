<?php 
  $title='Register';
  require_once 'includes/header.php';
?>
  <body style="background-image: url(build/images/backgrounds/16.jpg)" class="body-bg-full">
    <div class="container page-container">
      <div class="page-content">
        <div class="logo"><img src="build/images/logo/logo-sm-light.png" alt="" width="80"></div>
        <form method="get" action="index.php" class="form-horizontal">
          <div class="form-group">
            <div class="col-xs-12">
              <input type="text" placeholder="Username" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input type="text" placeholder="Email" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input type="password" placeholder="Password" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input type="password" placeholder="Confirm Password" class="form-control">
            </div>
          </div>
          
          <button type="submit" class="btn-lg btn btn-primary btn-rounded btn-block">Sign up</button>
        </form>
        <hr>
        <div class="clearfix">
          <p class="text-muted mb-0 pull-left">Already have an account?    </p><a href="login.php" class="inline-block pull-right">Sign In</a>
        </div>
      </div>
    </div>
    <!-- Change background image start-->
    <div class="setting closed"><a href="javascript:;" class="setting-toggle fs-16"><i class="ti-palette text-white"></i></a>
      <h5 class="fs-16 mt-0 mb-20 text-white">Background Images</h5>
      <ul class="list-inline">
        <li><a href="javascript:;" data-bg="14.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/14.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="15.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/15.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="16.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/16.jpg" width="60" alt="" class="img-rounded"></a></li>
      </ul>
      <ul class="list-inline mb-0">
        <li><a href="javascript:;" data-bg="17.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/17.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="18.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/18.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="19.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/19.jpg" width="60" alt="" class="img-rounded"></a></li>
      </ul>
    </div>
    <!-- Change background image end-->
<?php
  require_once 'includes/reg_scripts.php';
  require_once 'includes/footer.php';
?>