<?php 
  $title='Forgot Password';
  $style=' style="height: 100%"';
  require_once 'includes/header.php';
?>
  <body style="background-image: url(build/images/backgrounds/16.jpg)" class="body-bg-full">
    <div class="container page-container">
      <div class="page-content">
        <div class="logo"><img src="build/images/logo/logo-sm-light.png" alt="" width="80"></div>
        <h4 class="fs-16 text-white fw-300 mt-0">Forgot Password</h4>
        <p class="text-muted">Enter the email address associated with your account to reset your password</p>
        <form method="get" action="index.php">
          <div class="form-group">
            <input type="text" placeholder="Enter Email" class="form-control">
          </div>
          <button type="submit" style="width: 130px" class="btn btn-primary btn-rounded">Reset</button>
        </form>
      </div>
    </div>
    <!-- Demo Settings start-->
    <div class="setting closed"><a href="javascript:;" class="setting-toggle fs-16"><i class="ti-palette text-white"></i></a>
      <h5 class="fs-16 mt-0 mb-20 text-white">Background Images</h5>
      <ul class="list-inline">
        <li><a href="javascript:;" data-bg="14.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/14.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="15.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/15.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="16.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/16.jpg" width="60" alt="" class="img-rounded"></a></li>
      </ul>
      <ul class="list-inline mb-0">
        <li><a href="javascript:;" data-bg="17.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/17.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="18.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/18.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="19.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/19.jpg" width="60" alt="" class="img-rounded"></a></li>
      </ul>
    </div>
    <!-- Demo Settings end-->
<?php
  require_once 'includes/reg_scripts.php';
  require_once 'includes/footer.php';
?>