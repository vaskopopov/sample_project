<?php 
  session_start();
  
  //if user is already logged in, he can't see the login page until he logs out
  //just redirect him to index.php
  if(isset($_SESSION['user'])) {
    header("Location: index.php");
    die();
  }

  $title='Login';
  require_once 'includes/header.php';
  $msg = "";

  //if he is trying to login, check the credentials
  if($_SERVER['REQUEST_METHOD'] == "POST" && $_POST['btn_login']=="login") {
    require_once 'classes/database.class.php';
    require_once 'classes/users.class.php';
    require_once 'classes/redirect.class.php';
    $user = new User($_POST);
    $databaseOperations = new Db;
   
    $email = $user->getEmail();
    $password = $user->getPassword();

    $row = $databaseOperations->getUserData($email, $password); 
    
    
    if($row) {
      // if he got the right credentials, set SESSION info and redirect him to index.php
      // I will add an array in the $_SESSION['users'] with the info we will need accross the site to display user info
      
      $user->setId(['id' => $row['id_users']]);
      $user->setFirstname(['firstname' => $row['firstname']]);
      $user->setLastname(['lastname' => $row['lastname']]);
      $user->setEmail(['email' => $row['email']]);
      $user->setCode(['code' => $row['code']]);
      $user->setUserTypeId(['user_type_id' => $row['user_type_id']]);
      $user->setIsAdmin(['is_admin' => $row['is_admin']]);
      $user->setCompanyId(['company_id' => $row['company_id']]);
      $user->setCreatedAt(['created_at' => $row['created_at']]);

      
      $_SESSION['user'] = $user->getInfo(); //$_SESSION['user']-> [[0]=>[['id']=>'1'],[1]=>[['firstname']=>'NAME'],.....]

      foreach($user->getInfo() as $key=>$val){
        foreach($val as $k=>$v){
          $_SESSION['user_'][$k]=$v; //$_SESSION['user_']-> [['id']=>['1'],['firstname']=>['NAME'],.....]
        } // 'user_' is used in addNewUser.php
      }
      //*-*------------------------
      // var_dump($user->getInfo());
      // var_dump($_SESSION);
      // var_dump($_SESSION['user']);
      // var_dump($_SESSION['user_']);
      // die();

      //---------------------------
      Redirect::to('index.php');
      die();
    } else {
      //else just set a msg variable that will be used to show him an error message
      $msg = "Incorrect username or password";
    }
    
  }
?>
  <body style="background-image: url(build/images/backgrounds/16.jpg)" class="body-bg-full">
    <div class="container page-container">
      <div class="page-content">
        <div class="logo"><img src="build/images/logo/logo-sm-light.png" alt="" width="80"></div>
        <form method="post" action="login.php" class="form-horizontal">
          <div class="form-group">
            <span class="label label-outline label-danger"><?=$msg?></span>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input type="text" name="email" placeholder="Username or Email" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input type="password" name="password" placeholder="Password" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">             
              <div class="pull-right"><a href="forgot-password.php" class="inline-block form-control-static">Forgot Passowrd?</a></div>
            </div>
          </div>
          <button type="submit" class="btn-lg btn btn-primary btn-rounded btn-block" name="btn_login" value="login">Sign in</button>
        </form>      
        <hr>
        <div class="clearfix">
          <p class="text-muted mb-0 pull-left">Want new account?</p><a href="register.php" class="inline-block pull-right">Sign Up</a>
        </div>
      </div>
    </div>
    <!-- Demo Settings start-->
    <div class="setting closed"><a href="javascript:;" class="setting-toggle fs-16"><i class="ti-palette text-white"></i></a>
      <h5 class="fs-16 mt-0 mb-20 text-white">Background Images</h5>
      <ul class="list-inline">
        <li><a href="javascript:;" data-bg="14.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/14.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="15.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/15.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="16.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/16.jpg" width="60" alt="" class="img-rounded"></a></li>
      </ul>
      <ul class="list-inline mb-0">
        <li><a href="javascript:;" data-bg="17.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/17.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="18.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/18.jpg" width="60" alt="" class="img-rounded"></a></li>
        <li><a href="javascript:;" data-bg="19.jpg" class="inline-block body-bg"><img src="build/images/thumbnails/19.jpg" width="60" alt="" class="img-rounded"></a></li>
      </ul>
    </div>
    <!-- Demo Settings end-->
<?php
  require_once 'includes/reg_scripts.php';
  require_once 'includes/footer.php';
  session_destroy();
?>